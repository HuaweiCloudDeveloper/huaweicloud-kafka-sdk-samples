## 项目介绍
Kafka是一个拥有高吞吐、可持久化、可水平扩展，支持流式数据处理等多种特性的分布式消息流处理中间件，采用分布式消息发布与订阅机制，在日志收集、流式数据传输、在线/离线系统分析、实时监控等领域有广泛的应用。

### 开发环境
<table>
    <tr style="font-weight: bolder"><td>工具</td><td>版本</td></tr>
    <tr><td>JDK</td><td>1.8</td></tr>
    <tr><td>IDEA</td><td>2021.3.1</td></tr>
    <tr><td>Maven</td><td>3.8.1</td></tr>
    <tr><td>Kafka</td><td>2.3.0</td></tr>
</table>

### 场景说明
Kafka作为一个中间件组件，具有与其他中间件组件通用的功能 （异步处理、系统解耦、流量削峰、日志处理），
但在某些特殊的功能方面，每个中间件拥有其独特的特性，其中Kafka作为一个具有高吞吐、高性能的中间件，
它也有其不足的地方，在某些应用场景下面要求中间件实现消息延时的功能，但Kafka本身是不具备这种能力的。

此DEMO项目实现了每条消息实现自定义的延时。

### 时序图
![](img/Kafka消息延时时序图.png)

### 参数指南
本项目中起到延时作用的类Delay.java其余类为官方提供用于测试生产和消费消息，
如需使用官方测试的使用的生产消费代码相关配置介绍可以参考https://support.huaweicloud.com/devg-kafka/how-to-connect-kafka.html 。
如需使用自己配置的生产者消费者，只配置Delay.java中的参数即可。
#### Delay.java参数详情
1. delay:自定义延时时间，单位ms。
2. topic_delay变量：用于临时存储消息的topic名称。
3. topic_out变量：用于消费者拉取消息消费的topic名称。
4. 关于消费者和生产者配置可按需配置，可参考Kafka官方文档：https://kafka.apache.org/documentation/#producerconfigs

### 调用结果
![](./img/延时结果.png)

### 问题反馈通道
https://support.developer.huaweicloud.com/feedback/