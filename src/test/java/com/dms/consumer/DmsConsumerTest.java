package com.dms.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.junit.Test;

import java.util.Collections;

public class DmsConsumerTest {
    @Test
    public void testConsumer() throws Exception {
        DmsConsumer consumer = new DmsConsumer();//获取自定义消费者对象DmsConsumer。
        consumer.consume(Collections.singletonList("topic_out"));//设置需要消费Topic。
        try {
            while(true) {
                ConsumerRecords<Object, Object> records = consumer.poll(1000);//拉取消息的频率，单位：次/毫秒。
                //遍历消息。
                for (ConsumerRecord<Object, Object> record : records) {
                    System.out.println("消费topic_out消息："+record.value());//输出每条具体的消息。
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            consumer.close();
        }
    }
}
