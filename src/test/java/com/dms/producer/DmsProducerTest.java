package com.dms.producer;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.Test;

public class DmsProducerTest {
    @Test
    public void testProducer() {
        DmsProducer<String, String> producer = new DmsProducer<>();

        try {
            int i =1;
            while (true){
                Thread.sleep(1000);
                String data = "生产" + (i++);//设置生产的数据
                //
                producer.produce("topic_delay", data, new Callback()
                {
                    public void onCompletion(RecordMetadata metadata,
                                             Exception exception)
                    {
                        if (exception != null)
                        {
                            exception.printStackTrace();
                            return;
                        }
                    }
                });
                System.out.println("produce msg:" + data);
            }
        }catch (Exception e)
        {
            // TODO: 异常处理
            e.printStackTrace();
        }finally {
            producer.close();
        }
    }
}
